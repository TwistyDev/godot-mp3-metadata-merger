extends Control

var mp3_file_path
var cover_file_path

var OUTPUT_PATH = "" # YOUR OUTPUT PATH HERE

onready var mp3_path_label = get_node("TabContainer/Song/HBoxContainer/VBoxContainer/Mp3PathLabel")
onready var cover_texture_node = get_node("TabContainer/Song/HBoxContainer/AlbumCover")
onready var song_name_linedit_node = get_node("TabContainer/Song/HBoxContainer/VBoxContainer/SongNameLineEdit")

func _ready():
	get_tree().connect("files_dropped", self, "_get_dropped_files")

func _get_dropped_files(filepaths, screen):
	for filepath in filepaths:
		if ".mp3" in filepath:
			mp3_file_path = filepath
			mp3_path_label.text = filepath
		if ".jpg" in filepath:
			cover_file_path = filepath
			var cover_file = File.new()
			cover_file.open(filepath, File.READ)
			var img = Image.new()
			img.load_jpg_from_buffer(cover_file.get_buffer(cover_file.get_len()))
			var texture = ImageTexture.new()
			texture.create_from_image(img)
			cover_texture_node.texture = texture

func _on_Button_pressed():
	var song_title = song_name_linedit_node.text
	var output = []
	var error = OS.execute("ffmpeg", ["-i",mp3_file_path,"-i",cover_file_path,"-y","-map", "0:0", "-map" ,"1:0" ,"-c", "copy", "-id3v2_version" ,"3" ,"-metadata:s:v", "title='Album cover'", "-metadata:s:v", "comment='Cover (front)'", OUTPUT_PATH+song_title+".mp3"],true,output)
	if error == 0:
		mp3_path_label.text = "SUCCESS"
#	ffmpeg -i in.mp3 -i box.jpg -map 0:0 -map 1:0 -c copy -id3v2_version 3 -metadata:s:v title="Album cover" -metadata:s:v comment="Cover (front)" out.mp3
