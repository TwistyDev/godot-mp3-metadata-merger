# Godot mp3 metadata writer

![Screenshot](demo_cover.png)

A simple Godot app to write metadata to an mp3 file.  
Not meant for real use, made as an example for a youtube video.